# Maintainer: Mark Wagie <mark@manjaro.org>
# Contributor: Stefano Capitani <stefanoatmanjarodotorg>
# Contributor: Fabian Bornschein <fabiscafe-at-mailbox-dot-org>

pkgname=game-devices-udev
pkgver=0.23
pkgrel=1
pkgdesc="Udev rules for controllers"
url="https://codeberg.org/fabiscafe/game-devices-udev"
arch=('any')
license=('MIT')
depends=('udev')
makedepends=('git')
provides=('nintendo-udev')
install="$pkgname.install"
source=("git+https://codeberg.org/fabiscafe/game-devices-udev.git#tag=$pkgver?signed"
        'uinput.conf')
sha256sums=('d64670d26b8f49b3593cbec9286ae84a15fe0a99afcf19f8e647dafae9cc0b7d'
            'a771c9695d7283f7771adc00b680bd27391e6ac00e9fd026f4796067ee9a87eb')
validpgpkeys=('6E58E886A8E07538A2485FAED6A4F386B4881229') # Fabian Bornschein <fabiscafe@mailbox.org>

pkgver() {
  cd "$pkgname"
  git describe --tags | sed 's/-/+/g'
}

package() {
  cd "$pkgname"
  # install license
  install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname/"

  # install udev rules
  install -Dm644 *.rules -t "$pkgdir/usr/lib/udev/rules.d/"

  # start uinput at boot
  install -Dm644 "$srcdir/uinput.conf" -t "$pkgdir/usr/lib/modules-load.d/"
}
